**Репозиторий с лабораторными работами по дисциплине "Операционные системы и системное программирование" группы ПО-3**

---
После всех действий, описанных ниже, студент допускается к защите.
То есть, вы сначала сбрасываете лабы, я их проверяю, указываю на ошибки или "плохие" места. И вот только после этого студент подходит и защищается.

---
## Необходимые ссылки на codestyle guides
1. https://www.perforce.com/resources/qac/high-integrity-cpp-coding-standard
2. https://users.ece.cmu.edu/~eno/coding/CppCodingStandard.html
3. http://translatedby.com/you/google-c-style-guide/into-ru/?page=7

---

## Особенности личных профилей на bitbucket.org и варианты лабораторных работ
Профиль должен содержать в семе фамилию и имя, написанные латиницей, к примеру, *Lavruschik Artem*. Бранчи должны начинаться как *lab#_SurnameName*, где *#* - номер лабораторной.  

---

## Лабораторные работы

### Ссылка на методички
https://drive.google.com/folderview?id=1jmjafVy9P-8DCP1jq80phP5Xaqk_oLpq
#### Иерархия укладывания лабораторных 
Каждый человек должен сделать некоторый порядок выкладывания лабораторных работ в следующем виде: *lab1_2_SurnameName* -> от нее отбранчевывается *lab3_SurnameName* -> ... -> *lab7_SurnameName*  

---

## Что нужно сделать в самом начале

Установить утилиту **git** себе на компьютер (полезные ссылки ниже).  
1. https://git-scm.com/book/ru/v1/%D0%92%D0%B2%D0%B5%D0%B4%D0%B5%D0%BD%D0%B8%D0%B5-%D0%A3%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%BA%D0%B0-Git  
2. https://www.youtube.com/watch?v=Q181djTJeBU  


---
## Копирование репозитория себе на компьютер

1. Открываете консоль
2. В консоли переходите командами в любую удобную папку для хранения
3. Пишете *git clone git@bitbucket.org:lwwwr/po-3-osisp.git*
4. Вуаля, репозиторий скопирован.

---
## Следующие действия при выполнении лабораторной работы
1. Создаёте, так называемый, *branch* с именем вида *ФамилияИмя*, к примеру, *LavruschikArtem*(или *LavruschikArtem*, если совмещенная). Делаете это по ссылке https://bitbucket.org/lwwwr/po-3-osisp/branches/ где жмёте кнопочку "Create Branch", type *Other*, from branch *master*
2. После создания бранча, Вам выдаст команду для локального чека. Исходя из примера, команда будет вида *git fetch && git checkout LavruschikArtem*. Её необходимо ввести через консоль, находясь в папке с репозиторием.
3. Выполняете лабораторную работу.
4. Копируете в соответствии с иерархией файлы с кодом в нужную папку в репозитории.
5. Убеждаетесь, что нет лишних файлов командой *git status*
6. Далее в консоли пишете **"git add *"** 
7. **"git commit -m 'lab_НомерЛабы_ФамилияИмя Какое-нибудь сообщение(обязательно на английском, заодно подтянете его немножко)'"** 
8. **git push**
9. Если всё сделаете правильно, консоль выдаст сообщение с ссылкой на создание "Pull request". Копируете в браузер и переходите по ссылке. Откроется страница для заполнения *Description*(здесь нужно описать что вы сделали и указать свой вариант). Ссылка будет вида https://bitbucket.org/lwwwr/po-3-osisp/pull-requests/new?source=LavruschikArtem&t=1
10. Нажимаете *Create pull request*
11. Ждёте, пока я одобрю запрос.

---
## Перед началом выполнения следующей лабораторной
1. В консоли *git checkout master*
2. *git pull*
3. А дальше действия, которые описаны выше(про бранч, создание и прочее)


---
## Дополнительная информация
Прежде чем задавать вопрос - убедитесь, что вопрос неглупый и существует в гугле на него решение. Здесь описано некоторое количество информации. Самообразование никто не отменял.


